package edu.aubg.istanbul;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class LocationMenu extends AppCompatActivity {
    private RecyclerView rec;
    private RecyclerView.Adapter adapter;
    private List<Item> items;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_menu);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        rec = (RecyclerView) findViewById(R.id.holder);
        rec.setLayoutManager(new LinearLayoutManager(this));
        items = new ArrayList<>();
        String text = "testo";
        int video = R.raw.pls;
        ArrayList<Integer> test = new ArrayList<>();
        test.add(R.drawable.derp);
        test.add(R.drawable.index);
        Item item = new Item("test1", R.drawable.index,text,video,test);
        items.add(item);
        item = new Item("test7",R.drawable.derp,"this is an experement",R.raw.thingie,test);
        String name = "Basilica Cistern";
        int image = R.drawable.maxresdefault;
        String desc = "The Basilica Cistern (Turkish: Yerebatan Sarnıcı – \"Cistern Sinking Into Ground\"), is the largest of several hundred ancient cisterns that lie beneath the city of Istanbul (formerly Constantinople), Turkey. The cistern, located 150 metres (490 ft) southwest of the Hagia Sophia on the historical peninsula of Sarayburnu, was built in the 6th century during the reign of Byzantine Emperor Justinian I.The Basilica Cistern (Turkish: Yerebatan Sarnıcı – \"Cistern Sinking Into Ground\"), is the largest of several hundred ancient cisterns that lie beneath the city of Istanbul (formerly Constantinople), Turkey. The cistern, located 150 metres (490 ft) southwest of the Hagia Sophia on the historical peninsula of Sarayburnu, was built in the 6th century during the reign of Byzantine Emperor Justinian I.";
        int videos = R.raw.thingie;
        ArrayList<Integer> images = new ArrayList<>();
        images.add(R.drawable.maxresdefault);
        Item item2 = new Item(name,image,desc,videos,images);
        items.add(item2);
        adapter = new ItemAdapter(items, this);
        rec.setAdapter(adapter);
    }
}
