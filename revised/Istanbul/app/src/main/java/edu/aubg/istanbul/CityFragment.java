package edu.aubg.istanbul;
;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class CityFragment extends Fragment {
    VideoView videov;

    MediaController memedia;
    ArrayList<Integer> imglist;
    ImageAdapter imgada;
    TextView textView;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(
                R.layout.fragment_city, container, false);
        videov = (VideoView) view.findViewById(R.id.videoview);

        textView = (TextView) view.findViewById(R.id.fragtext);
        imglist= this.getArguments().getIntegerArrayList("Pictures");
        int video = this.getArguments().getInt("Video");
        String text = this.getArguments().getString("Text");
        String path = "android.resource://" + getActivity().getPackageName() + "/" +video;
        textView.setText(text);
        Uri uri = Uri.parse(path);
        videov.setVideoURI(uri);
        videov.seekTo(1000);
        final Boolean doneonce = false;
        videov.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!doneonce ) {
                    memedia = new MediaController(getContext());
                    videov.setMediaController(memedia);
                    memedia.setAnchorView(videov);
                    videov.start();
                }
            }
        });

        GridView gridview = (GridView) view.findViewById(R.id.gridview);
        imgada = new ImageAdapter(getContext(),imglist);
        gridview.setAdapter(imgada);


        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Fragment frag = new Imagefrag();
                Bundle bondle = new Bundle();
                bondle.putInt("pic",imglist.get(position));
                frag.setArguments(bondle);
                FragmentManager fragger = getFragmentManager();

                FragmentTransaction fragtrans = fragger.beginTransaction();
                fragtrans.add(R.id.fragter,frag);
                fragtrans.commit();
            }
        });

        return view;

    }

}