package edu.aubg.istanbul;

import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dave on 04/03/2018.
 */

public class Item {
    private String Name,Text;
    private int Image,Video;
    private ArrayList<Integer> piclist;

    public Item(String name, int image, String text, int video,ArrayList<Integer> list) {
        Name = name;
        Image = image;
        Text = text;
        Video = video;
        piclist = list;
    }

    public String getName() {
        return Name;
    }

    public String getText() {
        return Text;
    }

    public int getVideo() {
        return Video;
    }

    public ArrayList<Integer> getPiclist() {
        return piclist;
    }

    public int getImage() {
        return Image;
    }
}
